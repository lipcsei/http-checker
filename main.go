package main

import (
	"bufio"
	"fmt"
	"io"
	"log"
	"net/url"
	"strings"

	"net/http"
	"os"
	"sync"
)

// semaphoreLimit is the maximum number of concurrent goroutines to prevent system overload.
const semaphoreLimit = 10

const envInputFilePath = "INPUT_FILE"

func main() {

	// Read input path from env
	inputFilePath := os.Getenv(envInputFilePath)
	if inputFilePath == "" {
		log.Printf("The %s env is missing!\n", envInputFilePath)
		return
	}

	// Read urls from an input file
	urlList, err := readURLsFromFile(inputFilePath)
	if err != nil {
		log.Println(err)
		return
	}

	// Create an url set to avoid the duplicates
	urlSet := map[string]bool{}
	for _, url := range urlList {
		urlSet[url] = true
	}

	// Create a semaphore to limit concurrent goroutines
	semaphore := make(chan bool, semaphoreLimit)

	// Initialize a WaitGroup to wait for all goroutines to finish
	var wg sync.WaitGroup

	// Create a channel for the responses
	resultChannel := make(chan Result, len(urlSet))

	// Start goroutines for each URL
	for url := range urlSet {
		wg.Add(1)

		// Use a semaphore to limit concurrent goroutines and prevent system overload
		semaphore <- true

		go call(url, &wg, resultChannel, semaphore)
	}

	// Wait for all goroutines to finish
	go func() {
		wg.Wait()
		close(resultChannel)
	}()

	// Print results to stdout
	for result := range resultChannel {
		if result.Err != nil {
			log.Printf("Error checking URL (%s): %v\n", result.Url, result.Err)
		} else {
			log.Printf("URL: %s, Status: %s\n", result.Url, result.Status)
		}
	}

}

// Result stores the URL and its corresponding status code
type Result struct {
	Url    string
	Status string
	Err    error
}

// call sends an HTTP GET request to the specified URL and sends the result to the channel
func call(url string, wg *sync.WaitGroup, resultChannel chan<- Result, sem chan bool) {
	defer wg.Done()

	// Release the semaphore after the function exits
	defer func() { <-sem }()

	// Send an HTTP GET request to the URL
	resp, err := http.Get(url)
	if err != nil {
		if os.IsTimeout(err) {
			resultChannel <- Result{Url: url, Err: fmt.Errorf("timeout error")}
		}
		resultChannel <- Result{Url: url, Err: err}
		return
	}

	// Close the response body
	defer func(Body io.ReadCloser) {
		if err := Body.Close(); err != nil {
			log.Printf("Error closing response body: %v\n", err)
		}
	}(resp.Body)

	// Send the result to the channel
	resultChannel <- Result{Url: url, Status: resp.Status}

	return
}

// readURLsFromFile reads urls from an input file
func readURLsFromFile(filePath string) (result []string, err error) {
	// Open the input file
	file, err := os.Open(filePath)
	if err != nil {
		return nil, fmt.Errorf("Error opening input file: %v\n", err)
	}

	// Close the input file
	defer func(file *os.File) {
		if err := file.Close(); err != nil {
			log.Printf("Error closing input file: %v\n", err)
		}
	}(file)

	// Use a scanner to read each line from the file
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		// Trim whitespaces from the URL
		rawUrl := strings.TrimSpace(scanner.Text())
		if rawUrl == "" {
			continue
		}
		var path *url.URL
		path, err = url.ParseRequestURI(rawUrl)
		if err != nil {
			log.Printf("%s is an invalid url!\n", rawUrl)
			continue
		}

		result = append(result, path.String())
	}

	// Check for scanner errors
	if err = scanner.Err(); err != nil {
		return nil, fmt.Errorf("Error reading input file: %v\n", err)
	}

	return
}
