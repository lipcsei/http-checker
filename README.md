## Feladat:
HTTP Állapotkód Ellenőrző Szolgáltatás Konténerizált Alkalmazásként

## Cél:
Készíts egy Go nyelven írt alkalmazást, ami aszinkron módon ellenőrzi különböző weboldalak HTTP állapotkódjait és konténerben futtatható.

## Leírás:
Az alkalmazás fogadjon egy URL listát (pl. egy fájlból, környezeti változóból vagy konfigurációs fájlból).
Minden URL-t egy külön goroutine-nak kell feldolgoznia.
Minden goroutine küldjön egy HTTP GET kérést a megadott URL-re, és olvassa ki az HTTP válasz állapotkódját.
Az eredményeket (URL és az állapotkód) gyűjtse össze egy központi helyen, használva channel-eket a goroutine-ok és a gyűjtő közötti kommunikációra.
Az alkalmazás kezelje megfelelően a hibákat, mint például érvénytelen URL-ek, nem elérhető szerverek, vagy túllépési idők (timeouts).

## Konténerizálás:
Az alkalmazás legyen csomagolva Docker konténerbe.
Készíts egy Dockerfile-t, ami leírja, hogyan kell a Go alkalmazást konténerbe csomagolni.
Az alkalmazásnak úgy kell futnia a konténerben, hogy a különböző konfigurációs beállításokat (pl. URL lista elérési útvonala) környezeti változókon keresztül lehessen megadni.
pl: docker run -v $(pwd)/url.txt:/url.txt -e CONFIG_FILE=/url.txt http-checker
de lehet máshogyan is, pl használthatsz compose-t is


## Speciális Követelmények:
A hibakezelésnek érthető üzeneteket kell visszaadnia a felhasználónak.
A goroutine-ok és channel-ek használata legyen hatékony és jól strukturált.
A program legyen képes kezelni nagyszámú URL-t anélkül, hogy túlterhelné a rendszert vagy a konténert - semafor.


# Megoldás

## Buildelés

```
docker build -t http-checker .     
```
## Futtatás
```
docker run -v $(pwd)/input.txt:/app/input.txt -e INPUT_FILE=/input.txt http-checker   
```

## Futtatás docker compose használatával

```
docker-compose up
```
majd
```
docker-compose down
```