# Dockerfile

# Use the official Golang image
FROM golang:latest

# Set the working directory inside the container
WORKDIR /app

# Copy the local package files to the container's workspace
COPY . .

# Build the Go application
RUN go build -o http-checker .

# Set environment variable
ENV INPUT_FILE /app/input.txt

# Command to run the executable
CMD ["./http-checker"]